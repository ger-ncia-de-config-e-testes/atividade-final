# Atividade Final



## Sobre

- Atividade Final da Residência TCE-RN 2023.1
- Displina IMD0179 - TÓPICOS ESPECIAIS EM DESENVOLVIMENTO DE SOFTWARE 2 - T01 (2023.1)
- Professor: Dr. Gustavo Leitão

## Alunos envolvidos

- Andreza Soares da Silva
- Renan Lima Gomes
- Suzyanne Xavier Marciel 


## Apresentação da Atividade

Crie um repositório no Gitlab que satisfaça os seguintes critérios:

1. branches: main e develop
2. Só permita realizar push na branch `main` e `develop` através de MR
3. Crie um pipeline para, pelo menos:
    - Compilação/Build
    - Execute testes
    - Validação de qualidade
    - Criação de imagem Docker
    - Publicação da imagem no registro ou publicação da biblioteca
    - Deploy
 
 # Regras
   - Grupo de 3 pessoas
   - Entrega 01 de Abril  
   - Enviar link do repositório com video de até 3 minutos explicando o trabalho realizado.

